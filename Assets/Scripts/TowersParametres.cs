using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowersParametres : MonoBehaviour
{
    private Sprite tempSpriteShot;
    [SerializeField] private SpriteRenderer sp;
    public Stat JuniorChef;
    public Stat SeniorChef;
    public Stat SousChef;
    
    public static TowersParametres Instance { get; private set; }//синглтон
    private string language;
    private string tempName;
    
    public Shots broccoli;
    public Shots pashot;
    public Shots paella;
    public Shots pekinDuck;
   
    
    private void Awake()
    {
        Instance = this;//синглтон
    }

    void Start()
    {
        language = PlayerPrefs.GetString("Language");//Младший повар
        if (language == "RU")
            tempName = GameManager.Instance.RuDictionary.juniorChef;
        else if (language == "EN")
            tempName = GameManager.Instance.EnDictionary.juniorChef;
        JuniorChef=new Stat(1,tempName,50,5,5,5f,1.2f,0,
            false,0,false,0);
      
        Debug.Log("JC"+JuniorChef.cost+"Lvl"+JuniorChef.level+"name "+JuniorChef.name+"max "+JuniorChef.maxDamage+
                  "crit"+JuniorChef.critChance+"dist"+JuniorChef.distance);
        
        if (language == "RU")//Сеньор шеф
            tempName = GameManager.Instance.RuDictionary.seniorChef;
        else if (language == "EN")
            tempName = GameManager.Instance.EnDictionary.seniorChef;
        SeniorChef=new Stat(2,tempName,80,6,11,5.75f,0.8f,0,
            false,0,false,0);
        
        if (language == "RU")//Су-шеф
            tempName = GameManager.Instance.RuDictionary.sousChef;
        else if (language == "EN")
            tempName = GameManager.Instance.EnDictionary.sousChef;
        SousChef=new Stat(3,tempName,110,11,17,6.5f,0.7f,0,
            false,0,false,0);


        foreach (var tempSprite in GameManager.Instance.spritesOfGame)
        {
            if (tempSprite.name=="broccoli")
            {
                tempSpriteShot = tempSprite;
            }
        }
        broccoli=new Shots("broccoli", tempSpriteShot, 0,0,false,0,
            0,0,0,0,0,0 );
       
    }

   public class Stat
   {
       public int level = 1;
       public Sprite sprite;
       public string name;
       public int minDamage;
       public int maxDamage;
       public float distance; // дальность башни
       public int cost; //стоимость башни
       public float critChance = 0; //шанс крита
       public bool splash = false; //возможность сплэш урона
       public float splashRadius = 0; // радиус сплэш урона (о - если нет)
       public float speed; //время между выстрелами в сек
       public bool stunChance = false; //возможность стана
       public float stunTime = 0; //время стана (0 если стана нет)
       public Stat(int level,string name, int cost, int minDamage,int maxDamage, float distance, float speed, 
           float critChance, bool splash, float splashRadius, bool stunChance, float stunTime)
       {
           this.level = level;
           this.cost=cost;
           this.name = name;
           this.minDamage = minDamage;
           this.maxDamage = maxDamage;
           this.distance = distance;
           this.speed = speed;
           this.critChance = critChance;
           this.splash = splash;
           this.splashRadius = splashRadius;
           this.stunChance = stunChance;
           this.stunTime = stunTime;
       }
   }
   
   public class Shots
   {
       public string nameOfShot;
       public Sprite shotSprite;
       public int additiveDamage;
       public int splashRadius;
       public bool chain;
       public float chainChance;
       public int chainNumbers;
       public float fullSleepChance;
       public float percentDamage;
       public int level;
       public int reloadTime;
       public int poisonTime;

       public Shots(string nameOfShot, Sprite shotSprite, int additiveDamage, int splashRadius, bool chain,
           float chainChance, int chainNumbers, float fullSleepChance, float percentDamage, int level, 
           int reloadTime, int poisonTime)
       {
           this.nameOfShot = nameOfShot;
           this.shotSprite = shotSprite;
           this.additiveDamage = additiveDamage;
           this.splashRadius = splashRadius;
           this.chain = chain;
           this.chainChance = chainChance;
           this.chainNumbers = chainNumbers;
           this.fullSleepChance = fullSleepChance;
           this.percentDamage = percentDamage;
           this.level = level;
           this.reloadTime = reloadTime;
           this.poisonTime = poisonTime;

       }
   }
   
}
