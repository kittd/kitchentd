using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapClose : MonoBehaviour
{
   [HideInInspector] [SerializeField] private Animator capAnimator;
    public void capClose()
    {
        capAnimator.SetBool("Cap",false);
    }
}
