using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class TowerController : MonoBehaviour
{
    [SerializeField] public ColliderScaner colliderScaner;
    [HideInInspector]public string towerName;
    void Start()
    {
        towerName = colliderScaner.TowerName;
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
