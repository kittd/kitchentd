using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }//синглтон
    [HideInInspector]  [SerializeField] private RUDictionary ruDictionary;
    public RUDictionary RuDictionary => ruDictionary;//тексты на русском языке
    
    [HideInInspector] [SerializeField] private ENDictionary enDictionary;
    public ENDictionary EnDictionary => enDictionary;// Тексты на английском языке

    [HideInInspector] public string language;
    [SerializeField] public List<GameObject> TowersTypes;// список типа башен
    [SerializeField] public List<TowerStat> TowerStats1Lvl;
    [SerializeField] public ChoisePanelManager choisePanelManager;// панель 1 стройки башен
    [SerializeField] public GameObject scolarReviewPanel;//панель жалобы школьника
    [HideInInspector] public Text scolarReviewText;//
    [HideInInspector]public GameObject placeForReviews;//Текст жалобы школьника
    public GameObject upgradePanel2items;
    public FollowWorldCor upgradePanel2Follow;
    [HideInInspector] public GameObject AcceptPanel;
    [HideInInspector] public TextMeshProUGUI AcceptText;
    [SerializeField] public Text GoldCount;//текстовое поле золота на уровне

    [Header("Количество золота на старте уровня")] [SerializeField] public int gold=150;
    [SerializeField] public Text HealhtBar;
    [Header("Количество здоровья на этом уровне")][SerializeField]private int healthPoint;
    [Header("Количество волн на этом уровне")][SerializeField] public int wavesCount;//количество волн на уровне
    
    [Header("Путь монстров")][SerializeField] public GameObject[] waypoints;// Список точек пути

    public Dictionary<GameObject, Health> healthConteiner;//контейнер здоровья врагов
    [HideInInspector] public List<Bullet> buletsPool;//пул пуль повара (№1)

    [SerializeField] public Transform bulletPoolPoint;//пул пуль
    [HideInInspector] public int currentHealhtPlayer;//текущее здоровье игрока 
    [SerializeField] private Image[] StarsImage;
    [SerializeField] private Sprite[] StarsSprites;
    public List<TowersParametres.Stat> stats;
    public Sprite[] spritesOfGame;
     private Sprite tempSprite;
    private string tempName;
  
   
  

    #region Towers
   
    public TowerStat seniorChef;
    public TowerStat sousChef;

   #endregion
    void Awake()
    {
        if(!PlayerPrefs.HasKey("Language"))
            PlayerPrefs.SetString("Language", "EN");
        Instance = this;//синглтон
        healthConteiner=new Dictionary<GameObject, Health>();//инициация
        buletsPool=new List<Bullet>();//инициация
        spritesOfGame = Resources.FindObjectsOfTypeAll<Sprite>();
        
    }
 
    private void Start()
    {
        upgradePanel2items.SetActive(false);
        stats=new List<TowersParametres.Stat>();
        currentHealhtPlayer = healthPoint; 
        language = PlayerPrefs.GetString("Language");//инициация языка
        goldRefresh();
        RefreshStrings();
        ReviewsStarsRefresh();
    }

   

    void Update()
    {
  
     

    }

    public void onClickTower1()//метод стройки башни тип 1
    {
        if (gold<TowersParametres.Instance.JuniorChef.cost)
            return;
        var currentPlaceTower = choisePanelManager.currentTowerPlace;
        var CurrentTower=Instantiate(TowersTypes[0], currentPlaceTower.transform.position, quaternion.identity);
        CurrentTower.SetActive(true);
        CurrentTower.GetComponent<TowerController>().colliderScaner.StatInit(TowersParametres.Instance.JuniorChef);
        CurrentTower.GetComponent<TowerController>().colliderScaner.NextStatInit(TowersParametres.Instance.SeniorChef);
        gold -= TowersParametres.Instance.JuniorChef.cost;
        goldRefresh();
        Destroy(currentPlaceTower);
        choisePanelManager.gameObject.SetActive(false);
    }
    public void onClickTower2()//метод стройки башни тип 2
    {
        if (gold<TowerStats1Lvl[1].cost)
            return;
        var currentPlaceTower = choisePanelManager.currentTowerPlace;
        var CurrentTower=Instantiate(TowersTypes[1], currentPlaceTower.transform.position, quaternion.identity);
        CurrentTower.SetActive(true);
        gold -= TowerStats1Lvl[1].cost;
        goldRefresh();
        Destroy(currentPlaceTower);
        choisePanelManager.gameObject.SetActive(false);
    }
    public void onClickTower3()//метод стройки башни тип 3
    {
        if (gold<TowerStats1Lvl[2].cost)
            return;
        var currentPlaceTower = choisePanelManager.currentTowerPlace;
        var CurrentTower=Instantiate(TowersTypes[2], currentPlaceTower.transform.position, quaternion.identity);
        CurrentTower.SetActive(true);
        gold -= TowerStats1Lvl[2].cost;
        goldRefresh();
        Destroy(currentPlaceTower);
        choisePanelManager.gameObject.SetActive(false);
    }
    public void onClickTower4()//метод стройки башни тип 4
    {
        if (gold<TowerStats1Lvl[3].cost)
            return;
        var currentPlaceTower = choisePanelManager.currentTowerPlace;
        var CurrentTower=Instantiate(TowersTypes[3], currentPlaceTower.transform.position, quaternion.identity);
        CurrentTower.SetActive(true);
        gold -= TowerStats1Lvl[3].cost;
        goldRefresh();
        Destroy(currentPlaceTower);
        choisePanelManager.gameObject.SetActive(false);
    }
    public void onClickTowerType1Lvl2()//метод апгрейда башни тип 1 до лвл2
    {
        if (gold<TowersParametres.Instance.SeniorChef.cost)
            return;
        var currentPlaceTower = choisePanelManager.currentTowerPlace;
        var CurrentTower=Instantiate(TowersTypes[0], currentPlaceTower.transform.position, quaternion.identity);
        CurrentTower.SetActive(true);
        CurrentTower.GetComponent<TowerController>().colliderScaner.StatInit(TowersParametres.Instance.JuniorChef);
        gold -= TowersParametres.Instance.JuniorChef.cost;
        goldRefresh();
        Destroy(currentPlaceTower);
        choisePanelManager.gameObject.SetActive(false);
    }
    public void languageChange()
    {
        if (language=="EN")
            PlayerPrefs.SetString("Language","RU");
        else PlayerPrefs.SetString("Language", "EN");
    }

    public void RefreshStrings()
    {
        if (language == "EN")
        {
            scolarReviewText.text = enDictionary.scolarReview;//жалоба школьника
            HealhtBar.text = enDictionary.healthBar+" "+ currentHealhtPlayer + "/" + healthPoint;;
            GoldCount.text = enDictionary.gold + " : " + gold;
        }
        else if (language == "RU")
        {
            scolarReviewText.text = RuDictionary.healthBar; //Плохие отзывы
            HealhtBar.text = ruDictionary.healthBar+" "+ currentHealhtPlayer + "/" + healthPoint;//Bad Reviews;//жалоба школьника
            GoldCount.text = ruDictionary.gold + " : " + gold;
        }
        
    }

    public void goldRefresh()
    {
        if (language == "EN")
        {
           GoldCount.text = enDictionary.gold + " : " + gold;
        }
        else if (language == "RU")
        {
            GoldCount.text = ruDictionary.gold + " : " + gold;
        }
    }

    public void ReviewsStarsRefresh()
    {
        if (currentHealhtPlayer<20&&currentHealhtPlayer>=16)
        starChange(StarsImage[0]);
        else if (currentHealhtPlayer<16&&currentHealhtPlayer>=12)
            starChange(StarsImage[1]);
        else if (currentHealhtPlayer<12&&currentHealhtPlayer>=8)
            starChange(StarsImage[2]);
        else if (currentHealhtPlayer<8&&currentHealhtPlayer>=4)
            starChange(StarsImage[3]);
        else starChange(StarsImage[4]);
    }

    private void starChange(Image curentStar)
    {
        int i = 0;
        if (currentHealhtPlayer >= 16)
            i = 20 - currentHealhtPlayer;
        else if (currentHealhtPlayer >= 12 && currentHealhtPlayer < 16)
            i = 16 - currentHealhtPlayer;
        else if (currentHealhtPlayer >= 8 && currentHealhtPlayer < 12)
            i = 12 - currentHealhtPlayer;
        else if (currentHealhtPlayer >= 4 && currentHealhtPlayer < 8)
            i = 8 - currentHealhtPlayer;
        else i = 4 - currentHealhtPlayer;
        
        curentStar.sprite = StarsSprites[i];
    }
    
    
   
    public class TowerStat
    {
        public int towerLevel = 1;
        public Sprite sprite;
        public string name;
        public int minDamage;
        public int maxDamage;
        public float distance; // дальность башни
        public int cost; //стоимость башни
        public float critChance = 0; //шанс крита
        public bool splash = false; //возможность сплэш урона
        public float splashRadius = 0; // радиус сплэш урона (о - если нет)
        public float speed; //время между выстрелами в сек
        public bool stunChance = false; //возможность стана
        public float stunTime = 0; //время стана (0 если стана нет)

        public float StunTime
        {
            get => stunTime;
            set
            {
                if (stunChance)
                    stunTime = value;
                else stunTime = 0;
            }
        }

        public float armorPenetration; // в процентах 55,5 например

        public float ArmorPenetration
        {
            get => armorPenetration;
            set
            {
                if (ArmorPenetration < 100)
                    armorPenetration = value;
                else armorPenetration = 100;
            }

        }

        public TowerStat(int towerLVL, Sprite sprite, string name, int minDamage, int maxDamage, float distance,
            int cost, bool splash, float splashRadius, float speed, bool stunChance, float stunTime, float armorPenetration)
        {
            towerLevel = towerLVL;
            this.sprite = sprite;
            this.name = name;
            this.minDamage = minDamage;
            this.maxDamage = maxDamage;
            this.distance = distance;
            this.cost = cost;
            this.splash = splash;
            this.splashRadius = splashRadius;
            this.speed = speed;
            this.stunChance = stunChance;
            this.stunTime = stunTime;
            this.armorPenetration = armorPenetration;
        }

        public void VVV()
        {
            Debug.Log(cost);
        }
    
    }
}



