using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClickableObjectTower : MonoBehaviour//висит на самой башне
{
    private GameObject _upgradePanel2;
    private FollowWorldCor FWC;
    private Animator animator;
    [SerializeField] private ColliderScaner colliderScaner;//сканер текущей башни которую кликнули

    private void OnEnable()
    {
        _upgradePanel2 = GameManager.Instance.upgradePanel2items;
        FWC = GameManager.Instance.upgradePanel2Follow;
        animator = gameObject.GetComponent<Animator>();

    }

    public void SetUpgrade()//срабатывает при клике на любую башню
    {
        ButtonsController.Instance.upradeButton.onClick.RemoveAllListeners();
        ButtonsController.Instance.upradeButton.onClick.AddListener(UpgradeButtonClick);
        ButtonsController.Instance.saleButton.onClick.RemoveAllListeners();
        ButtonsController.Instance.saleButton.onClick.AddListener(SellButtonClick);
        ButtonsController.Instance.acceptButton.onClick.RemoveAllListeners();
        ButtonsController.Instance.acceptButton.onClick.AddListener(AcceptButtonClick);
        GameManager.Instance.choisePanelManager.gameObject.SetActive(false);
        _upgradePanel2.SetActive(true);
        FWC.lookAt = gameObject.transform;
        GameManager.Instance.AcceptText.text = colliderScaner.TowerName + "\n" + colliderScaner.damageText +
                                               colliderScaner.minDamage +
                                               " - " + colliderScaner.maxDamage + "\n" + colliderScaner.distanceText +
                                               colliderScaner.distance;
                                             
        colliderScaner._spriteRenderer.sprite=colliderScaner.auraSprite;
        Debug.Log("Press");
    }

    void UpgradeButtonClick()
    {
        GameManager.Instance.AcceptText.text = colliderScaner.nextTowerName + "\n" + colliderScaner.damageText +
                                               colliderScaner.nextMinDamage +
                                               " - " + colliderScaner.nextMaxDamage + "\n" + colliderScaner.distanceText +
                                               colliderScaner.nextDistance+"\n"+colliderScaner.costText+colliderScaner.nextCost;
        ButtonsController.Instance.acceptButton.gameObject.SetActive(true);
        ButtonsController.Instance.upradeButton.gameObject.SetActive(false);
    }

    void SellButtonClick()
    {
    }

    void AcceptButtonClick()
    {
        if (GameManager.Instance.gold<colliderScaner.nextCost)
            return;
        if (colliderScaner.TowerName == "Senior chef" || colliderScaner.TowerName == "Старший повар")//апгрейд повара до 3 уровня
        {
            GameManager.Instance.gold -= colliderScaner.nextCost;
            animator.SetBool("Lvl3", true);
            _upgradePanel2.SetActive(false);
            colliderScaner.StatInit(TowersParametres.Instance.SousChef);
            //ТУДУ добавить кукурузу, исправить пар
        }
        else if (colliderScaner.TowerName == "Junior chef" || colliderScaner.TowerName == "Младший повар")//апгрейд повара до 2 уровня
        {
            GameManager.Instance.gold -= colliderScaner.nextCost;
            GameManager.Instance.goldRefresh();
            animator.SetBool("Lvl2", true);
            _upgradePanel2.SetActive(false);
            colliderScaner.StatInit(TowersParametres.Instance.SeniorChef);
            colliderScaner.NextStatInit(TowersParametres.Instance.SousChef);
            ButtonsController.Instance.acceptButton.gameObject.SetActive(false);
            ButtonsController.Instance.upradeButton.gameObject.SetActive(true);
            
            return;
            //ТУДУ добавить кукурузу, исправить пар
        }
        
    }
}