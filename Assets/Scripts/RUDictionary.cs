using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RUDictionary : MonoBehaviour
{
    [HideInInspector] public string healthBar= "Плохие отзывы";
    [HideInInspector] public string scolarReview;
    [HideInInspector] public string gold;
    [HideInInspector] public string juniorChef="Младший повар";
    [HideInInspector] public string seniorChef="Старший повар";
    [HideInInspector] public string sousChef="Су-шеф";

    [HideInInspector] public string  damageText;
    [HideInInspector] public string distanceText;
    [HideInInspector] public string costText;

    public void Awake()
    {
        healthBar= "Плохие отзывы";
        scolarReview = "Я папе расскажу";
        gold = "Золото";
        juniorChef="Младший повар";
        seniorChef="Старший повар";
        damageText = "Урон: ";
        distanceText = "Дальность: ";
        costText = "Стоимость: ";
        sousChef="Су-шеф";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
