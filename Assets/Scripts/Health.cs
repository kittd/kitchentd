using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;


public class Health : MonoBehaviour
{
    [SerializeField] float  maxHP;
    [HideInInspector] public float healthPoint;
    public int goldForSleep;
    public float armorPercent;
    private Animator animator;
    float percentHealth;
    [HideInInspector] public SpriteRenderer sp;
    private float currentAlpha=1;
    void Start()
    {
        healthPoint = maxHP;
        GameManager.Instance.healthConteiner.Add(gameObject, this);
        animator = gameObject.GetComponent<Animator>();
        percentHealth = 1;
        animator.SetBool("NotFat", true);
        sp = gameObject.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

   public void HealthDamage(float damage)
   {
      
        if (percentHealth > 0.7f)//чавк худого
        {
            animator.SetBool("Eat", true);
          
        }
        else if (percentHealth > 0.40f && percentHealth < 0.70f)//чавк толстого
        {
            animator.SetBool("EatFat", true);
            
        }
        else if (percentHealth > 0 && percentHealth < 0.40f)//чавк оч толстого
        {
            animator.SetBool("EatVeryFat", true);
            
        }

        healthPoint -= damage;//нанесение урона
       percentHealth = healthPoint / maxHP;
       
        
        if (healthPoint<=0)//сон
        {
            animator.SetBool("Sleep", true);
            animator.SetBool("VeryFat", false);
            gameObject.GetComponent<MoveEnemy>().speed = 0;
            StartCoroutine(TimeToSleep());
            GameManager.Instance.gold += goldForSleep;
            GameManager.Instance.goldRefresh();
        }
        else if (percentHealth < 0.70f && percentHealth > 0.40f)//переход на толстый
        {
         
            animator.SetBool("Fat", true);
            animator.SetBool("NotFat", false);
           

        }
        else if (percentHealth < 0.40f && percentHealth > 0)//переход на оч толстый
        {
            animator.SetBool("VeryFat", true);
            animator.SetBool("Fat", false);
            
        }

       
        
    }

   IEnumerator TimeToSleep() //организация перезарядки
   {
       yield return new WaitForSeconds(0.02f);
       if (currentAlpha > 0)
       {
           currentAlpha -= 0.02f;
           sp.color = new Color(255, 255, 255, currentAlpha);
       }

       if (currentAlpha == 0)
       {
           GameManager.Instance.healthConteiner.Remove(gameObject);
           Destroy(gameObject);
       }

       StartCoroutine(TimeToSleep());
   }

   public void NotFat()//снятие чавка худого
   {animator.SetBool("Eat",false);
      
   }
   public void Fat()//снятие чавка толстого
   {animator.SetBool("EatFat",false);
      
   }
   
   public void VeryFat()//снятие чавка оч толстого
   {animator.SetBool("EatVeryFat",false);
       
   }

   public void StartFlip()//старт времемени до разворота цели
   {
       StartCoroutine(Flip());
   }

   IEnumerator Flip() //разворот цели 
   {
       yield return new WaitForSeconds(0.3f);
     sp.flipX = false;
   }
}
