using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [HideInInspector] public int additiveDamage;
    [HideInInspector] public float percentDamage;
    [HideInInspector] public float damage;
    [HideInInspector] public Sprite sprite;
    [HideInInspector] public float splashRadius=0;
    [HideInInspector] public float stunTime=0;
    [HideInInspector] public string nameOfShot;
    [HideInInspector] public Health target;
    public bool move ;
    [HideInInspector] public GameObject targetObject;
    private Vector2 targetPosition;
    private Rigidbody2D rb;
    [HideInInspector] public SpriteRenderer _spriteRenderer;
    private Vector2 bulletPosition;
    private Vector2 toMouth;
    private float timeInterval;

    private Animator animator;

    private float rotateAngle;
    // Start is called before the first frame update
    void Start()
    {
       rb = gameObject.GetComponent<Rigidbody2D>();
        damage += additiveDamage;
        damage = damage*(1 + percentDamage);
    }

    // Update is called once per frame
    void Update()
    {
        if (target.healthPoint<0)
            ReturnBulletToPool(this);
        if (move)
        {
            
            bulletPosition = gameObject.transform.position;
            targetPosition = targetObject.transform.position;
            var endPosition = new Vector2((targetPosition.x - bulletPosition.x) / 100,
                (targetPosition.y - bulletPosition.y) / 100);
            gameObject.transform.position = Vector2.Lerp (bulletPosition, targetPosition+toMouth, timeInterval*1*Time.deltaTime);
        }
        gameObject.transform.Rotate(new Vector3(0,0,rotateAngle));
        rotateAngle += 0.0025f;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        
    }

    private void OnEnable()
    {   
    }
    IEnumerator TimeToDie() //организация перезарядки
    {
        yield return new WaitForSeconds(5f);
        ReturnBulletToPool(gameObject.GetComponent<Bullet>());
        
    }
    IEnumerator TimeToMove() //организация перезарядки
    {
        yield return new WaitForSeconds(0.5f);
        move = true;
        rb.isKinematic = true;
        timeInterval = Vector2.Distance(gameObject.transform.position, targetObject.transform.position);
        
    }
    public void ReturnBulletToPool(Bullet bulletTemp)//возврат пули в пул
    {
        
        if (!GameManager.Instance.buletsPool.Contains(bulletTemp))
            GameManager.Instance.buletsPool.Add(bulletTemp);
        var bulletsPool = GameManager.Instance.bulletPoolPoint;
        bulletTemp.transform.parent = bulletsPool;
        bulletTemp.transform.position = bulletsPool.transform.position;
        bulletTemp.rb.isKinematic = false;
        bulletTemp.gameObject.SetActive(false);
    }

    public void newStart()
    {
        move = false;
        StartCoroutine(TimeToMove());
        toMouth=new Vector2(0.1f,0.1f);
        StartCoroutine(TimeToDie());
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (GameManager.Instance.healthConteiner.ContainsKey(col.gameObject))
        {
            if (target==GameManager.Instance.healthConteiner[col.gameObject])
            {
                if (bulletPosition.x<targetPosition.x)
                {
                    target.sp.flipX = true;//разворот цели к пуле
                    target.StartFlip();//разворот цели через время 
                }

                
                target.HealthDamage(damage);
                ReturnBulletToPool(this);
            }
        }
    }
}

