using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Unity.VisualStudio.Editor;
using UnityEngine;

public class PlaceTowwer : MonoBehaviour
{
    [SerializeField] private List<GameObject> towers;
    [HideInInspector] public GameObject currentTower;
    [SerializeField] private GameObject towersPanelChoise;
    private FollowWorldCor _followWorldCor;

  
    // Start is called before the first frame update
    void Start()
    {
        towersPanelChoise = GameManager.Instance.choisePanelManager.gameObject;
        _followWorldCor = towersPanelChoise.GetComponent<FollowWorldCor>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool canPlaceTower()
    {
        return currentTower == null;
        
    }

    public void TowerStay()
    {
        if (canPlaceTower())
        {
            GameManager.Instance.upgradePanel2items.SetActive(false);
            towersPanelChoise.GetComponent<ChoisePanelManager>().currentTowerPlace = gameObject;
           _followWorldCor.lookAt = gameObject.transform;
          
           if (towersPanelChoise.activeInHierarchy)
           towersPanelChoise.SetActive(false);
           else
           towersPanelChoise.SetActive(true);
        }
    }


    
}
