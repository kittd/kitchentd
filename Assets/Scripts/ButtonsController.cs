using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonsController : MonoBehaviour
{
   public static ButtonsController Instance { get; private set; }//синглтон

   public Button upradeButton;
   public Button saleButton;
   public Button acceptButton;

   private void Awake()
   {
      Instance = this;
   }
}
