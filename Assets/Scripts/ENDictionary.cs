using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.WSA;

public class ENDictionary : MonoBehaviour
{
    [HideInInspector] public string healthBar= "Bad rewiews";
    [HideInInspector] public string scolarReview;
    [HideInInspector] public string gold;
    [HideInInspector] public string juniorChef;
    [HideInInspector] public string seniorChef;
    [HideInInspector] public string sousChef;
    [HideInInspector] public string  damageText;
    [HideInInspector] public string distanceText;
    [HideInInspector] public string costText;
   
   public void Awake()
   {
       healthBar = "Bad rewiews";
       scolarReview = "I'll tell Dad";
       gold = "Gold";
       juniorChef = "Junior chef";
       seniorChef = "Senior chef";
       sousChef = "Sous chef";
       damageText = "Damage: ";
       distanceText = "Distance: ";
       costText = "Cost: ";

   }

    // Update is called once per frame
    void Update()
    {
        
    }
}
