using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Serialization;


public class ColliderScaner : MonoBehaviour
{
    private GameObject _parent;//ссылка на сам объект башни
    private List<Health> TargetList;//массив целей
    private bool isReadyShoot;//возможность стрельбы
    private Transform bulletsPool;//точка выстрела
    private Bullet bulletTemp;//текущая пуля
   // private TowerStat _towerStat;//ссылка 
    [SerializeField] Bullet  bullet;
    [SerializeField] private Transform bulletShootPoint;
    private Health lastTarget;//последняя цель
    [SerializeField] private Animator capAnimator;//анимтор крышки
    [SerializeField] private AudioClip capShoot;//звук выстрела
    private Transform _transform;//ссылка на координаты башни 
    [HideInInspector]public TowersParametres.Stat currentTowerStat;//Полные статы текущей башни

    #region статы текущей башни
    [HideInInspector] public int level = 1;
    [HideInInspector] public Sprite sprite;
    [HideInInspector] public string TowerName;
    [HideInInspector]public int minDamage;
    [HideInInspector]public int maxDamage;
    [HideInInspector]public float distance; // дальность башни
    [HideInInspector]public int cost; //стоимость башни
    [HideInInspector] public float critChance = 0; //шанс крита
    [HideInInspector] public bool splash = false; //возможность сплэш урона
    [HideInInspector]public float splashRadius = 0; // радиус сплэш урона (о - если нет)
    [HideInInspector]public float speed; //время между выстрелами в сек
    [HideInInspector]public bool stunChance = false; //возможность стана
    [HideInInspector] public float stunTime = 0; //время стана (0 если стана нет)
    #endregion
    
    #region статы башни следующего уровня до 3
    [HideInInspector] public int nextLevel = 1;
    [HideInInspector] public Sprite nextSprite;
    [HideInInspector] public string nextTowerName;
    [HideInInspector]public int nextMinDamage;
    [HideInInspector]public int nextMaxDamage;
    [HideInInspector]public float nextDistance; // дальность башни
    [HideInInspector]public int nextCost; //стоимость башни
    [HideInInspector] public float nextCritChance = 0; //шанс крита
    [HideInInspector] public bool nextSplash = false; //возможность сплэш урона
    [HideInInspector]public float nextSplashRadius = 0; // радиус сплэш урона (о - если нет)
    [HideInInspector]public float nextSpeed; //время между выстрелами в сек
    [HideInInspector]public bool nextStunChance = false; //возможность стана
    [HideInInspector] public float nextStunTime = 0; //время стана (0 если стана нет)
    #endregion
    
    public SpriteRenderer _spriteRenderer;
    [SerializeField] public Sprite auraSprite;

    #region Texts

    [HideInInspector] public string damageText;
    [HideInInspector]public string distanceText;
    [HideInInspector]public string costText;
    
    

    #endregion
    
    private void Awake()
    {
        
    }

    void Start()
    {

        _spriteRenderer.sprite = null;
        _transform = gameObject.GetComponent<Transform>();
        _transform.localScale = new Vector3(distance,distance,1);
        bulletsPool = GameManager.Instance.bulletPoolPoint;
        isReadyShoot = true;//перезаряжен?
        _parent = transform.parent.gameObject;//ссылка на объект башни
        TargetList = new List<Health>();
        BulletInit();
      
        
    }

    private void OnEnable()
    {
        InitDictionary();
       
    }

    private void OnTriggerEnter2D(Collider2D other)//занесение объекта в список целей при пеерсечении коллайдера
    {
        if (GameManager.Instance.healthConteiner.ContainsKey(other.gameObject))
        {
            TargetList.Add(GameManager.Instance.healthConteiner[other.gameObject]);
        }
    }

    private void OnTriggerExit2D(Collider2D other)//удаление объекта из списка целей при выходе из коллайдера
    {
        if (GameManager.Instance.healthConteiner.ContainsKey(other.gameObject))
        {
            TargetList.Remove(GameManager.Instance.healthConteiner[other.gameObject]);
           
        }
    }

    

    private void Update()
    {
       
        if (isReadyShoot && TargetList.Count!=0)
        {
            CheckShoot();
        }
    }

   

    void BulletInit()//добавление пуль в пул
    {
        
        var N = 5 * speed; for (int i = 0; i < N; i++)
        {
            bulletTemp = Instantiate(bullet, bulletsPool);
            bulletTemp.gameObject.SetActive(false);
        }
    }

    private Bullet GetBulletFromPool()//взятие пули из пула
    {
        if (GameManager.Instance.buletsPool.Count > 0) 
        {
            Bullet bulletTemp = GameManager.Instance.buletsPool[0];
            GameManager.Instance.buletsPool.Remove(bulletTemp);
            bulletTemp.gameObject.SetActive(true);
            bulletTemp.transform.parent = null;
            bulletTemp.transform.position = bulletShootPoint.transform.position;// координаты старта выстрела
           
            return bulletTemp;
        } 
        else 
            return Instantiate(bullet, bulletsPool);
        
    }
    
    void CheckShoot()//организация выстрела
    {
        bulletTemp = GetBulletFromPool();
        BulletChoise();
        bulletTemp.damage =UnityEngine.Random.Range (minDamage, maxDamage+1);//change
       
        bool isRight;
        var currentTarget = TargetList[0];//взятие первой цели из таргетлиста
        bulletTemp.target = currentTarget;
        if (currentTarget.healthPoint <= bulletTemp.damage)
            TargetList.Remove(currentTarget);
        bulletTemp.move = false;
        bulletTemp.targetObject = currentTarget.gameObject;
        bulletTemp.newStart();
        var currentTargetPosition = currentTarget.gameObject.transform.position;
        if (currentTargetPosition.x > bulletTemp.gameObject.transform.position.x)
            isRight = true;
        else isRight = false;
        if (isRight)
        {
            var direction=new Vector2(0.2f,1);
            capAnimator.SetBool("Cap",true);
            bulletTemp.GetComponent<Rigidbody2D>().AddForce(direction*6, ForceMode2D.Impulse);
          
          isReadyShoot = false;
          StartCoroutine(IsReloading());
        }
        else
        {
            var direction=new Vector2(-0.2f,1);
            bulletTemp.GetComponent<Rigidbody2D>().AddForce(direction * 6, ForceMode2D.Impulse);
            capAnimator.SetBool("Cap",true);
            StartCoroutine(IsReloading());
        }

        isReadyShoot = false;
      
        StartCoroutine(IsReloading());
   
    }

    public void StatInit(TowersParametres.Stat stat)
    {
    level = stat.level;
    TowerName=stat.name;
    minDamage=stat.minDamage;
    maxDamage=stat.maxDamage;
    distance=stat.distance; // дальность башни
    cost=stat.cost; //стоимость башни
    critChance = stat.critChance; //шанс крита
    splash = stat.splash; //возможность сплэш урона
    splashRadius =stat.splashRadius; // радиус сплэш урона (о - если нет)
    speed=stat.speed; //время между выстрелами в сек
    stunChance = stat.stunChance; //возможность стана
    stunTime = stat.stunTime; //время стана (0 если стана нет)
    }

    public void NextStatInit(TowersParametres.Stat stat)
    {
        nextLevel = stat.level;
        nextTowerName=stat.name;
        nextMinDamage=stat.minDamage;
        nextMaxDamage=stat.maxDamage;
        nextDistance=stat.distance; // дальность башни
        nextCost=stat.cost; //стоимость башни
        nextCritChance = stat.critChance; //шанс крита
        nextSplash = stat.splash; //возможность сплэш урона
        nextSplashRadius =stat.splashRadius; // радиус сплэш урона (о - если нет)
        nextSpeed=stat.speed; //время между выстрелами в сек
        nextStunChance = stat.stunChance; //возможность стана
        nextStunTime = stat.stunTime; //время стана (0 если стана нет)
    }

    void BulletChoise()
    {
        if (TowerName == "Младший повар" || TowerName == "Junior chef")
        {
            bulletTemp.sprite = TowersParametres.Instance.broccoli.shotSprite;
            bulletTemp.additiveDamage = TowersParametres.Instance.broccoli.additiveDamage;
            bulletTemp.percentDamage = TowersParametres.Instance.broccoli.percentDamage;
            bulletTemp.nameOfShot = TowersParametres.Instance.broccoli.nameOfShot;
        }

    }

    void InitDictionary()
    {
        if (GameManager.Instance.language == "EN")
        {
            damageText = GameManager.Instance.EnDictionary.damageText;
            distanceText = GameManager.Instance.EnDictionary.distanceText;
            costText = GameManager.Instance.EnDictionary.costText;
        }
        else if (GameManager.Instance.language == "RU")
        {
            damageText = GameManager.Instance.RuDictionary.damageText;
            distanceText = GameManager.Instance.RuDictionary.distanceText;
            costText = GameManager.Instance.RuDictionary.costText;
        }
    }

    IEnumerator IsReloading() //организация перезарядки
    {
        yield return new WaitForSeconds(speed);
        isReadyShoot = true;
    }

    
}
