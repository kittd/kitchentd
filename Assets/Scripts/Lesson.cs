
using UnityEngine;

public class Lesson5 : MonoBehaviour
{
    void Start()
    {
        Cat VaskaCat=new Cat();
        VaskaCat.Meow();
        Cat Murca=new Cat("Murka", 7, 5,31,27);
        Murca.Meow();
        Bus Ikarus=new Bus();
        Ikarus.Beep();
        Auto Logan=new Auto();
        Logan.Beep();
    }

}

abstract class Vehicle
{
    private string name;
    public abstract void Beep();
}

class Bus: Vehicle
{
    public override void Beep()
    {
        
        Debug.Log("BeBep");
    }
}
class Tractor: Vehicle
{
    public override void Beep()
    {
        
        Debug.Log("Beeeeep");
    }
}
class Auto: Vehicle
{
    public override void Beep()
    {
        
        Debug.Log("BeBeBep");
    }
}
class Cat
{
    public string name;
    public  int ageYears;
    private int weightKg;
    public int heightCm;
    public int tailLenghtCm;
    private string owner;
    public string Owner => owner;

    public int WeightKg
    {
        get => weightKg;
        set {
            if (value>0&& value<30)
            {
                weightKg = value;
            }
        }
    }

    public Cat()
    {
        name = "Vaska";
        ageYears = 2;
        weightKg = 6;
        heightCm = 25;
        tailLenghtCm = 35;
    }

    public Cat(string name, int ageYears, int weightKg, int heightCm, int tailLenghtCm)
    {
        this.name = name;
        this.ageYears = ageYears;
        this.weightKg = weightKg;
        this.heightCm = heightCm;
        this.tailLenghtCm = tailLenghtCm;
    }

    public void Meow()
    {
        Debug.Log("Меня зовут - " + name + ", мой рост - " + heightCm + "см, мне - "+ageYears+" лет, мой вес - "
                  + weightKg+" кг, длина моего хвоста - "+tailLenghtCm);
    }
}