using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Pointer : MonoBehaviour
{
   // Start is called before the first frame update
   private bool Closer;
   private GameObject data;
    void Start()
    {
       
    }

    private void OnMouseDown()
    {
        Closer = true;
        UIcontroller();
        if(data!=null)
            return;
        
        int layerObject = 8;
        Vector2 ray = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
        RaycastHit2D[] hit = Physics2D.RaycastAll(ray, ray, layerObject);
        if (hit.Length >0)
        {
            foreach (var VARIABLE in hit)
            {
                if (VARIABLE.collider.gameObject.CompareTag("TowerPlace"))
                {
                    VARIABLE.collider.gameObject.GetComponent<PlaceTowwer>().TowerStay();
                    Closer = false;
                }

                else if (VARIABLE.collider.gameObject.CompareTag("Tower"))
                {
                    VARIABLE.collider.gameObject.GetComponent<ClickableObjectTower>().SetUpgrade();
                    Closer = false;
                }
             else if (VARIABLE.collider.gameObject.layer == 5)
                    Closer = false;
            }
          
        }
       if(Closer)
        {
            GameManager.Instance.AcceptPanel.SetActive(false);
            GameManager.Instance.choisePanelManager.gameObject.SetActive(false);
            GameManager.Instance.upgradePanel2items.SetActive(false);
        }
    }
    void UIcontroller()
    {
       
            data = PointerRaycast(Input.mousePosition); // пускаем луч

            if(data != null)
            {
                Closer = false;
                //Debug.Log(data); // объект куда попал луч
               // Debug.Log(data.transform.parent); // родитель этого объекта
                return;
            }
        
    }

    GameObject PointerRaycast(Vector2 position)
    {
        PointerEventData pointerData = new PointerEventData(EventSystem.current);
        List<RaycastResult> resultsData = new List<RaycastResult>();
        pointerData.position = position;
        EventSystem.current.RaycastAll(pointerData, resultsData);

        if(resultsData.Count > 0)
        {
            return resultsData[0].gameObject;
        }

        return null;
    }
}
