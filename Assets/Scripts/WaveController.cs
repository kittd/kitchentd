using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Wave
{
    public GameObject enemyPrefab;
    public float spawnInterval = 2;
    public int maxEnemies = 20;
}

public class WaveController : MonoBehaviour
{
   [HideInInspector] private int currentWave=0;
    public Wave[] waves;
    [Header("Интервал между волнами в секундах")][SerializeField]  public int timeBetweenWaves = 5;
    private float lastSpawnTime;
    private int enemiesSpawned = 0;
    private bool WaveReload=false;
    void Start()
    {
        lastSpawnTime = Time.time;
    }

    void Update()
    {
        
        if (currentWave < waves.Length&& !WaveReload)
        {
            waweSpawn();
            // 4 
            if (enemiesSpawned == waves[currentWave].maxEnemies)
            {
                currentWave++;
                //gameManager.Gold = Mathf.RoundToInt(gameManager.Gold * 1.1f);
                enemiesSpawned = 0;
                lastSpawnTime = Time.time;
                WaveReload = true;
                StartCoroutine(waiting5Sec());
            }
            // 5 
        }
        /*else
        {
            //gameManager.gameOver = true;
            GameObject gameOverText = GameObject.FindGameObjectWithTag ("GameWon");
            gameOverText.GetComponent<Animator>().SetBool("gameOver", true);
        } */
    }

    void waweSpawn()
    {
        float timeInterval = Time.time - lastSpawnTime;
        float spawnInterval = waves[currentWave].spawnInterval;
        if (((enemiesSpawned == 0 && timeInterval > timeBetweenWaves) ||
             timeInterval > spawnInterval) && 
            enemiesSpawned < waves[currentWave].maxEnemies)
        {
            // 3  
            lastSpawnTime = Time.time;
            GameObject newEnemy = (GameObject)
                Instantiate(waves[currentWave].enemyPrefab);
            newEnemy.SetActive(true);
            //newEnemy.GetComponent<MoveEnemy>().waypoints = waypoints;
            enemiesSpawned++;
        }
    }

    IEnumerator waiting5Sec()
    {
        yield return new WaitForSeconds(timeBetweenWaves);
        WaveReload = false;
    }
}
