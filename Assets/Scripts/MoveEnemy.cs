using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveEnemy : MonoBehaviour
{
   
    [HideInInspector] private int currentWaypoint = 0;
    private float lastWaypointSwitchTime;
    [SerializeField] public float speed = 1.0f;
    // Start is called before the first frame update
    void Start()
    {
        lastWaypointSwitchTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if(speed==0)
            return;
        Vector3 startPosition = GameManager.Instance.waypoints [currentWaypoint].transform.position;
        Vector3 endPosition = GameManager.Instance.waypoints [currentWaypoint + 1].transform.position;
// 2 
        float pathLength = Vector3.Distance (startPosition, endPosition);
        float totalTimeForPath = pathLength / speed;
        float currentTimeOnPath = Time.time - lastWaypointSwitchTime;
        gameObject.transform.position = Vector2.Lerp (startPosition, endPosition, currentTimeOnPath / totalTimeForPath);
        
// 3 
        if ( gameObject.transform.position.x==endPosition.x && gameObject.transform.position.y==endPosition.y) 
        {
            if (currentWaypoint < (GameManager.Instance.waypoints.Length - 2))
            {
             
                currentWaypoint++;
                lastWaypointSwitchTime = Time.time;
             // TODO: поворачиваться в направлении движения
            }
            else//дошел до конца
            {
                StartCoroutine(TimeToReview());
                GameManager.Instance.currentHealhtPlayer--;
                GameManager.Instance.ReviewsStarsRefresh();
                GameManager.Instance.RefreshStrings();
                GameManager.Instance.scolarReviewPanel.GetComponent<FollowWorldCor>().lookAt = 
                    GameManager.Instance.placeForReviews.transform;
              if (gameObject!=null)
                GameManager.Instance.scolarReviewPanel.SetActive(true);
                Destroy(gameObject);

                //AudioSource audioSource = gameObject.GetComponent<AudioSource>();
                //AudioSource.PlayClipAtPoint(audioSource.clip, transform.position);
                // TODO: вычитать здоровье
            }
        }
    }

    IEnumerator TimeToReview() //время жалобы 
    {
        yield return new WaitForSeconds(1f);
       GameManager.Instance.scolarReviewPanel.SetActive(false);

       
    }
}
