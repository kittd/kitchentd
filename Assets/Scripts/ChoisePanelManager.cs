using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChoisePanelManager : MonoBehaviour
{
    [SerializeField]  List<Image> choisePanelOnPlaces;
    public List<Image> ChoisePanelOnPlaces// кнопки на панели пушки 1-4
    {
        get => choisePanelOnPlaces;
        set => choisePanelOnPlaces = value;
    }

    [SerializeField] private Sprite but1;
    [HideInInspector] public GameObject currentTowerPlace=null;//принимает в себя как объект место строительства откуда вызвана
    void Start()
    {
        for (int i = 1; i < 4; i++)
        {
            var tempSprite = GameManager.Instance.TowersTypes[i].GetComponent<SpriteRenderer>();//инициализация  списка Image картнками 1 уровня башен на панели строительства 
            choisePanelOnPlaces[i].sprite = tempSprite.sprite;
        }

        choisePanelOnPlaces[0].sprite = but1;

    }

    

    // Update is called once per frame
    void Update()
    {
        
    }
}
